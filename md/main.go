package main

import (
	"fmt"
	"github.com/yuin/goldmark"
	"os"
	"sort"
	"strings"
	"text/template"
	"time"
)

func wrapBp(s string, level ...int) string {
	relPath := ""
	if len(level) > 0 {
		l := level[0]
		for i := 0; i < l; i++ {
			relPath += "../"
		}
	}
	bp := `<!DOCTYPE html>
<html>
  <head>
    <meta name="robots" content="noindex,nofollow" />
    <meta charset="UTF-8">
    <title> Ajay R's Web Page </title>
    <link rel="stylesheet" href="%sstyle.css">
  </head>
  <body>
    <nav id="navbar">
      <a href="%sindex.html">Home</a>
      <a href="%swritings.html">Writings</a>
      <a href="%sinteresting_reads.html">Interesting Reads</a>
      <a href="%scontact.html">Contact</a>
    </nav>
      %s
  </body>
</html>`
	return fmt.Sprintf(bp, relPath, relPath, relPath, relPath, relPath, s)
}

var generic = wrapBp(`
<div>
  {{.Content}}
</div>`)

var wrtBp = wrapBp(`
<div>
  {{.Content}}
</div>`, 1)

type Content struct {
	Content string
}

func mdToHt(mdPath string, htPath string, t string) {
	tmpl, err := template.New("clever").Parse(t)
	if err != nil {
		panic(err)
	}
	data, err := os.ReadFile(mdPath)
	if err != nil {
		panic(err)
	}
	var b strings.Builder
	if err = goldmark.Convert(data, &b); err != nil {
		panic(err)
	}
	// Open ht
	f, err := os.Create(htPath)
	if err != nil {
		panic(err)
	}
	defer f.Close()
	err = tmpl.Execute(f, Content{b.String()})
	if err != nil {
		panic(err)
	}
}

func main() {
	// Writings
	f, err := os.Open("writings")
	defer f.Close()
	if err != nil {
		panic(err)
	}
	sf, err := f.ReadDir(0)
	if err != nil {
		panic(err)
	}
	type indexEl struct {
		t    time.Time
		text string
	}
	indexEls := make([]indexEl, len(sf))
	for _, v := range sf {
		if !v.IsDir() && strings.HasSuffix(v.Name(), ".md") {
			htName := strings.ReplaceAll(v.Name(), ".md", ".html")
			htName = strings.ReplaceAll(htName, "-", "_")
			// Filling writings.md
			b, err := os.ReadFile(fmt.Sprintf("writings/%s", v.Name()))
			if err != nil {
				panic(err)
			}
			sl := strings.Split(string(b), "\n")
			t := strings.ReplaceAll(sl[0], "# ", "") // first line = title
			d := strings.ReplaceAll(sl[2], "**", "") // third line = date
			parsed, err := time.Parse("January 2, 2006", d)
			if err != nil {
				panic(err)
			}
			indexEls = append(indexEls, indexEl{
				parsed,
				fmt.Sprintf("* %s [%s](writings/%s)\n", d, t, htName),
			})

			mdToHt(fmt.Sprintf("./writings/%s", v.Name()), fmt.Sprintf("../writings/%s", htName), wrtBp)
		}
	}
	sort.Slice(indexEls, func(p, q int) bool {
		return indexEls[q].t.Before(indexEls[p].t)
	})
	// Debugging:
	// fmt.Printf("sorted indexEls: %q", indexEls)
	wIndex := ""
	for _, v := range indexEls {
		wIndex += v.text
	}
	err = os.WriteFile("writings.md", []byte(wIndex), 0600)
	if err != nil {
		panic(err)
	}
	// Go through directory
	f, err = os.Open(".")
	defer f.Close() // Is there a problem here with the previous f.Close()?
	if err != nil {
		panic(err)
	}
	sf, err = f.ReadDir(0)
	if err != nil {
		panic(err)
	}
	for _, v := range sf {
		if !v.IsDir() && strings.HasSuffix(v.Name(), ".md") {
			htName := strings.ReplaceAll(v.Name(), ".md", ".html")
			htName = strings.ReplaceAll(htName, "-", "_")
			mdToHt(v.Name(), fmt.Sprintf("../%s", htName), generic)
		}
	}
}
