### Articles and Papers

* [Industrial Society and Its Future](https://web.cs.ucdavis.edu/~rogaway/classes/188/materials/Industrial%20Society%20and%20Its%20Future.pdf)
* [Hedonism, Asceticism and the Hermetic
  Answer](https://lukesmith.xyz/c/hedonism-asceticism-and-the-hermetic-answer)

  Luke's take against ascetisim in this blog post is interesting.

* [Write maintainable code the first
  time](https://attom.space/article/write-maintainable-code-the-first-time)

  In this immersive article, Tristan carefully deconstructs the difference
  between easy-to-read code and easy-to-maintain code.
* [The Internet: An Extension of
  Google](https://www.hydra.im/articles/the-internet-an-extension-of-google)

  An "SEO guy" bashes SEO.

### Books

* **Homo Deus** by Yuval Noah Harari

  This book is highly reminiscent of Theodore Kaczynski's seminal manifesto
  (linked above). It poses important questions about the future of humankind in
  the face of advancement of AI and biotechnology.

* **Privacy is Power** by Carissa Veliéz

  The title of the book is strangely hackneyed. The content, however, is not.
  After an initial description of the dystopian world we live in, Carissa
  proposes several—practical—legislative ideas to tackle privacy invasions.
  Overall, the book is deeply insightful.

* **The Brothers Karamazov** by Fyodor Dostoevsky

  TBK poses deep questions about our existence and its meaning through the use
  of characters that undergo profound trauma, moral suffering, despair, and
  struggles against their own worse selves. Dostoevsky did not live long enough
  to complete the two-part story: the second book was to continue
  Alyosha's—Dostoevsky's 'hero'—journey. The first part itself was exceedingly
  profound—I truly wonder what the second part would have been like, had
  Dostoevsky lived longer.
