# On Retail Stores Requiring Your Phone Number

**July 13, 2021**

Purchasing from retail stores used to be simple. You could walk in, grab what
you wanted, pay for them with cash, and walk out. Not anymore. Increasingly,
many stores are refusing to sell to those unwilling to give out their phone
numbers.

The first time this happened to me, I attributed their requirement for my phone
number to purely nefarious intentions—after all, who wouldn't want to profit by
selling phone numbers (and purchase histories) to marketing companies?

I've come to question the exclusivity of my attribution. Imagine you buy a
barbell rated with a load capacity of 200 kilograms. Later, the manufacturing
lead realizes that her unit-conversion team erred in the pound-to-kilogram
conversion, mistaking one pound for 1.2 kilograms. Your barbell's true load
capacity is 110 kilograms, not 200 kilograms! The lead then reaches out to her
retailers (to inform them of the error), who then reach out to their customers.
Without your phone number (or e-mail address), how would they get in touch with
you if something goes wrong?

Of course, not every product has associated risks. The socks you want to buy
aren't going to explode or snap apart. However, I can understand why retailers
would want to hedge themselves against potential lawsuits—'We informed you of
the defect as soon as we became aware of it.' And, well, this makes sense.

However, when you give them your phone number, they are not signing you up
solely for "you're-in-danger" alerts. They also sign you up for marketing
messages. In regard to this, there appears to be little choice—sign up for both,
or we'll refuse to sell to you.

There's one additional problem. Most of the IT companies that handle your data
for retailers are lax with security—the number of data breaches that occur every
day is staggering. These data breaches in turn become another source of spam,
phishing, and marketing messages. Don't we have enough of those already?  (Open
up your SMS app and count the spam you've received over the past week.)

As a general rule of thumb, assume that any information you give your retailer
is public information. Not wanting to share your phone number is probably
prudent—as long as you're willing to risk your barbell snapping in two. If your
retailer refuses to make a sale without your phone number, you could always make
up one—listen to 8765-309/Jenny for some great ideas.
